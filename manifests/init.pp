# Setup our helpy installation
class helpy {

  $helpy_deploy_password = trocla('helpy_production_deploy')

  Class['Ruby'] -> Class['rubygems']

  class { 'ruby': install_dev => true }

  include git
  include rubygems

  package { [ 'libpq-dev', 'imagemagick', 'curl', 'build-essential', 'zlib1g-dev', 'libssl-dev',
              'libreadline-dev', 'libyaml-dev', 'libcurl4-openssl-dev', 'libxml2-dev', 'bundler',
              'libxslt1-dev', 'software-properties-common', 'ruby-mail' ]: ensure => installed
  }

  group { 'helpy':
    ensure    => present,
    name      => 'helpy',
    allowdupe => false
  }

  user { 'helpy':
    gid     => helpy,
    home    => '/srv/helpy',
    require => Group['helpy'];
  }


  file {
    '/srv':
      ensure => directory,
      mode   => '0755',
      owner  => root,
      group  => root;

    '/etc/postfix/maps/discard_example':
      content => "example.com discard:\n",
      owner   => root,
      group   => root,
      mode    => '0644'
  }

  exec { '/usr/sbin/postmap /etc/postfix/maps/discard_example':
    refreshonly => true,
    subscribe   => File['/etc/postfix/maps/discard_example'];
  }

  class { 'postgresql::globals':
    version             => '11',
    server_package_name => 'postgresql'
  }
  -> class { 'postgresql::server': }

  postgresql::server::db { 'helpy':
    user     => 'helpy',
    password => postgresql_password('helpy', trocla('postgresql_helpy')),
  }

  class { 'apache':
    default_vhost     => false,
    default_ssl_vhost => false,
    mpm_module        => 'event'
  }

  apache::listen { '80': }
  apache::listen { '443': }

  apache::mod { 'removeip': package => 'libapache2-mod-removeip' }
  # needed because doing a apache::vhost::custom
  include apache::mod::ssl
  include apache::mod::passenger
  include apache::mod::proxy
  include apache::mod::proxy_balancer
  include apache::mod::rewrite
  include apache::mod::headers

  apache::vhost::custom {
    'riseuphelp':
      content => template('site_apache/vhosts.d/riseuphelp.riseup.net'),
  }

  # mpm_event
  # vhost template
  vcsrepo {
    '/srv/helpy':
      ensure   => latest,
      revision => 'riseup-2.8.0',
      provider => git,
      owner    => helpy,
      source   => "https://gitlab+deploy-token-1:${helpy_deploy_password}@code.revolt.org/tcp/helpy.git",
      notify   => [ Exec['helpy_bundler_update'] ],
      require  => [ User['helpy'], Group['helpy'], File['/srv'], Class['git'] ];
  }

  exec { 'helpy_bundler_update':
    cwd     => '/srv/helpy',
    command => '/usr/bin/bundle install --deployment --path vendor/bundle --without test development',
    unless  => '/usr/bin/bundle check --path vendor/bundle',
    user    => 'helpy',
    timeout => 600,
    require => [ Vcsrepo['/srv/helpy'] ],
    notify  => Service['apache2'];
  }

  augeas {
    'logrotate_helpy':
      context => '/files/etc/logrotate.d/helpy/rule',
      changes => [
        'set file /srv/helpy/log/production.log', 'set rotate 3',
        'set schedule weekly', 'set compress compress',
        'set delaycompress delaycompress', 'set missingok missingok',
        'set ifempty notifempty', 'set copytruncate copytruncate',
        'set create/mode 0440', 'set create/owner helpy',
        'set create/group helpy' ];
  }

  # modsecurity
  include apache::mod::security

  # use the recommended conffile, which is DetectionOnly
  file { '/etc/modsecurity/modsecurity.conf':
    ensure => link,
    target => '/etc/modsecurity/modsecurity.conf-recommended';
  }
}
